package com.kenfogel.workingwithconfig.config;

/**
 * @author Ken Fogel
 * @author Stephen He 1835417
 */
public class MailConfigBean {

    private String userName;
    private String emailAddress;
    private String emailPwd;
    private String imapUrl;
    private String imapPort;
    private String smtpUrl;
    private String smtpPort;
    private String mySqlName;
    private String mySqlUrl;
    private String mySqlPort;
    private String mySqlUname;
    private String mySqlPwd;

    /**
     * Default Constructor
     */
    public MailConfigBean() {
        userName = "";
        emailAddress = "";
        emailPwd = "";
        imapUrl = "";
        smtpUrl = "";
        imapPort = "993";
        smtpPort = "465";
        mySqlName = "";
        mySqlUrl = "";
        mySqlPort = "3306";
        mySqlUname = "";
        mySqlPwd = "";
    }

    /**
     * Non-default constructor that sets up all parameters
     *
     * @param host
     * @param userEmailAddress
     * @param password
     */
    public MailConfigBean(String userName, String emailAddress, String emailPwd, String imapUrl, String smtpUrl, String imapPort, String smtpPort, String mySqlName, String mySqlUrl, String mySqlPort, String mySqlUname, String mySqlPwd) {
        this.userName = userName;
        this.emailAddress = emailAddress;
        this.emailPwd = emailPwd;
        this.imapUrl = imapUrl;
        this.smtpUrl = smtpUrl;
        this.smtpPort = smtpPort;
        this.imapPort = imapPort;
        this.mySqlName = mySqlName;
        this.mySqlUrl = mySqlUrl;
        this.mySqlPort = mySqlPort;
        this.mySqlUname = mySqlUname;
        this.mySqlPwd = mySqlPwd;
    }

    //GETTERS AND SETTERS
    public String getUserName() {
        return userName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getEmailPwd() {
        return emailPwd;
    }

    public String getImapUrl() {
        return imapUrl;
    }

    public String getImapPort() {
        return imapPort;
    }

    public String getSmtpUrl() {
        return smtpUrl;
    }

    public String getSmtpPort() {
        return smtpPort;
    }

    public String getMySqlUrl() {
        return mySqlUrl;
    }

    public String getMySqlPort() {
        return mySqlPort;
    }

    public String getMySqlUname() {
        return mySqlUname;
    }

    public String getMySqlPwd() {
        return mySqlPwd;
    }

    public String getMySqlName() {
        return mySqlName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setEmailPwd(String emailPwd) {
        this.emailPwd = emailPwd;
    }

    public void setImapUrl(String imapUrl) {
        this.imapUrl = imapUrl;
    }

    public void setImapPort(String imapPort) {
        this.imapPort = imapPort;
    }

    public void setSmtpUrl(String smtpUrl) {
        this.smtpUrl = smtpUrl;
    }

    public void setSmtpPort(String smtpPort) {
        this.smtpPort = smtpPort;
    }

    public void setMySqlUrl(String mySqlUrl) {
        this.mySqlUrl = mySqlUrl;
    }

    public void setMySqlPort(String mySqlPort) {
        this.mySqlPort = mySqlPort;
    }

    public void setMySqlUname(String mySqlUname) {
        this.mySqlUname = mySqlUname;
    }

    public void setMySqlPwd(String mySqlPwd) {
        this.mySqlPwd = mySqlPwd;
    }

    public void setMySqlName(String mySqlName) {
        this.mySqlName = mySqlName;
    }
}
