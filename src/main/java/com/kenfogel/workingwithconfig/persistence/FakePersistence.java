package com.kenfogel.workingwithconfig.persistence;

import com.kenfogel.workingwithconfig.config.MailConfigBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author omni_
 */
public class FakePersistence {

    private static final Logger LOG = LoggerFactory.getLogger(FakePersistence.class);
    private final MailConfigBean configBean;
    private final String dbConnection;

    public FakePersistence(MailConfigBean configBean) {
        this.configBean = configBean;
        dbConnection = "jdbc:mysql//"
                + configBean.getMySqlUrl()
                + ":"
                + configBean.getMySqlPort()
                + "/"
                + configBean.getMySqlName()
                + "?characterEncoding=UTF-8&autoReconnect=true&zeroDateTimeBehavior=CONVERT_TO_NULL&useSSL=false&allowPublicKeyRetrieval=true";

    }

    public boolean getFakeData() {
        LOG.info("dbConnection= " + dbConnection);
        return true;
    }

}
