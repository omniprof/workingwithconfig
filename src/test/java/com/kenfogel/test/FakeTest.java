/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kenfogel.test;

import com.kenfogel.workingwithconfig.config.MailConfigBean;
import com.kenfogel.workingwithconfig.persistence.FakePersistence;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author omni_
 */
public class FakeTest {

    private static final Logger LOG = LoggerFactory.getLogger(FakeTest.class);

    private MailConfigBean configBean;
    private FakePersistence fakePersistence;

    @Before
    public void setup() {
        configBean = new MailConfigBean(
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                "AQUARIUM",
                "localhost",
                "3306",
                "fish",
                "kfstandard");
        fakePersistence = new FakePersistence(configBean);
    }

    @Test
    public void doFakeTest() {
        assertTrue(fakePersistence.getFakeData());

    }
}
